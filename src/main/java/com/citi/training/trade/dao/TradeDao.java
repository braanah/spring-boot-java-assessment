package com.citi.training.trade.dao;

import java.util.List;

import com.citi.training.trade.Trade;

/**
 * <h1>This is the Trade Data Access Object</h1>
 * The project uses this skeleton to access data.
 * @author Administrator
 *
 */
public interface TradeDao {
	List<Trade> findAll();

	Trade findById(int id);

	int create(Trade trade);

	void deleteById(int id);

}
