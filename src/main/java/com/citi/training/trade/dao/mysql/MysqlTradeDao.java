package com.citi.training.trade.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trade.Trade;
import com.citi.training.trade.dao.TradeDao;

/**
 * <h1>This is the SQL Data Access Object</h1>
 * This is used to run CRUD functionality on server data.
 * @author Administrator
 *
 */

@Component
public class MysqlTradeDao implements TradeDao{
	
	@Autowired
	JdbcTemplate tpl;
	
	public List<Trade> findAll() {
		return tpl.query("select * from trade",  new TradeMapper());
//		tpl.query
	}

	private static final class TradeMapper implements RowMapper<Trade>{
		public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Trade(rs.getInt("id"), rs.getString("stock"), rs.getDouble("price"), rs.getInt("volume"));
		}
	}
	
	public Trade findById(int id) {
		List<Trade> trades = this.tpl.query("select id, name, price from trade where id = ?",
				new Object[] { id }, new TradeMapper());
		if (trades.size() <= 0) {
			return null;
//			throw new TradeNotFoundException("No Trade found with id: " + id);
		}
		return trades.get(0);

	}

	public int create(Trade trade) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		this.tpl.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement("insert into trade (stock, price, volume) values (?, ?, ?)",
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, trade.getStock());
				ps.setDouble(2, trade.getPrice());
				ps.setDouble(3, trade.getVolume());
				return ps;
			}
		}, keyHolder);
		return keyHolder.getKey().intValue();

	}

	@Override
	public void deleteById(int id) {
		this.tpl.update("delete from trade where id=?", id);
	}
}
