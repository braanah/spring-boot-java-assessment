package com.citi.training.trade.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trade.Trade;
import com.citi.training.trade.service.TradeService;

/**
 * <h1>This is the main controller for the project</h1>
 * @author Administrator
 *
 */

@RestController
@RequestMapping("/trade")
public class TradeController {
	
//	public String BASE_PATH = "/trade";
	@Autowired
	TradeService tradeService;

	@RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Trade> findAll() {
		return tradeService.findAll();
	}

	@RequestMapping(value = "/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void create(@RequestBody Trade Trade) {
		tradeService.create(Trade);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Trade findById(@PathVariable int id) {
		return tradeService.findById(id);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteById(@PathVariable int id) {
		tradeService.deleteById(id);
	}

}
