package com.citi.training.trade.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.citi.training.trade.Trade;
import com.citi.training.trade.dao.TradeDao;

/**
 * <h1>This is the service that would be used to manipulate the data received from the server.</h1>
 * If we were going to make any business level changes to the data, that would happen here.
 * @author Administrator
 *
 */

@Component
public class TradeService {

	@Autowired
	TradeDao tradeDao;

	public List<Trade> findAll() {
		return tradeDao.findAll();
	}

	public Trade findById(@PathVariable int id) {
		return tradeDao.findById(id);
	}

	public int create(@RequestBody Trade trade) {
		return tradeDao.create(trade);
	}

	public void deleteById(@PathVariable int id) {
		tradeDao.deleteById(id);
	}

}