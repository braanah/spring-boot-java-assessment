package com.citi.training.trade;

import org.junit.Test;

public class TradeTest {
	
	@Test
	public void test_TradeConstructor() {
		Trade test1 = new Trade(1, "AAPL", 500, 4);
		assert(test1.getStock().equals("AAPL"));
		assert(test1.getPrice() == 500);
		assert(test1.getVolume() == 4);
	}
	
	@Test
	public void test_Setters() {
		Trade test1 = new Trade(1, "JBL", 250, 3);
		
		test1.setId(10);
		test1.setPrice(500);
		test1.setStock("AAPL");
		test1.setVolume(4);
		
		assert(test1.getId() == 10);
		assert(test1.getStock().equals("AAPL"));
		assert(test1.getPrice() == 500);
		assert(test1.getVolume() == 4);
	}
	

}
